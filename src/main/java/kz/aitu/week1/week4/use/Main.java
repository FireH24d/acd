package kz.aitu.week1.week4.use;
public class Main {
    public static void main(String[] args) {
        Stack st = new Stack();
        st.push(4);
        st.push(9);
        st.push(98);

        System.out.print("top is ");
        Node current = st.top;

        if (current != null) {
            System.out.print(current.data);

        }
        System.out.println();
        System.out.println("empty? " + st.empty());
        System.out.println("size is " + st.size());

        Node current2 = st.top;
        while (current2 != null) {
            System.out.print(current2.data + " ");
            current2 = current2.next;
        }
    }
}
