package kz.aitu.week1.week4.use;
public class Stack {
    Node top;

    public int pop(){
        Node popped;
        popped = top;
        top = top.next;
        return popped.data;

    }

    public void push(int data) {
        Node a = new Node(data);
        if (top == null) {
            top = a;
        } else {
            Node t=top;
             top=a;
            a.next = t;
        }

    }

    public boolean empty() {
        if (top == null) {
            return true;
        } else {
            return false;
        }
    }

    public int size() {
        Node current = top;
        int i=0;
        if (top == null) {
            return 0;
        } else {
            while (current != null) {
                current = current.next;
                i++;
            }
        }
        return i;
    }

    public int top() {
        int u = 0;
        while (top!=null){
            top =top.getNext();
            u++;
        }
        return u;

    }
}
