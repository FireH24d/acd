package kz.aitu.week1;
import java.util.Scanner;
public class labir {
    public static boolean findPath(int a[][],int i, int j) {
        if(a[i][j]==0) return false;
        if(i == 4 && j == 4) return true;
        boolean f = false;
        if(i+1!=5) f=f || findPath( a,i+1,j);
        if(j+1!=5) f=f || findPath(a,i,j+1);

        if(i-1!=-1) f=f || findPath( a,i-1,j);

        if(j-1!=-1) f=f || findPath(a,i,j-1);

        return f;
    }
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        int[][] array = new int[5][5];
        for(int i = 0; i < 5; i++){
            for(int j = 0; j < 5; j++){
                array[i][j] = input.nextInt();
            }
        }
        if(findPath(array,0,0)){
            System.out.println("Yes");
        }
        else   System.out.println("No");
    }
}