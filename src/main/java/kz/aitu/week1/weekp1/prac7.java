package kz.aitu.week1.weekp1;

import java.util.Scanner;

public class prac7 {

    static void printArray( int n)
    {
        if (n > 0)
        {
            Scanner scan = new Scanner(System.in);

            int s = scan.nextInt();
            printArray(n - 1);
            System.out.print(s+" ");
        }
        else return;
    }
    public static void main (String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();

        printArray(n);
    }
}
