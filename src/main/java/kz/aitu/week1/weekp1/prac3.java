package kz.aitu.week1.weekp1;

import java.util.Scanner;

public class prac3 {
    static boolean isPrime(int n)
    {
        if (n <= 1)
            return false;

        for (int i = 2; i < n; i++)
            if (n % i == 0)
                return false;

        return true;
    }

    public static void main(String args[])
    {
        Scanner scan = new Scanner(System.in);

        int num = scan.nextInt();

        if (isPrime(num))
            System.out.println(" Prime");
        else
            System.out.println(" Composite");

    }
}
