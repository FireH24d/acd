package kz.aitu.week1.weekp1;

import java.util.Scanner;
public class prac9 {
    static int binom(int n, int k)
    {
        if (k == 0 || k == n)
            return 1;
        return binom(n - 1, k - 1) +
                binom(n - 1, k);
    }
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int k = scan.nextInt();
        System.out.println(binom(n, k));
    }
}
