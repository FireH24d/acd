package kz.aitu.week1.weekp1;

import java.util.Scanner;
public class prac4 {
    static int factorial(int n)
    {
        if (n == 0)
            return 1;
        return n*factorial(n-1);
    }
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        int num = scan.nextInt();
        System.out.println( factorial(num));
    }
}
