package kz.aitu.week1.weekp1;

import java.util.Scanner;
public class prac8 {
    static boolean isNumber(String s)
    {
        for (int i = 0; i < s.length(); i++)
            if (Character.isDigit(s.charAt(i)) == false)
                return false;
        return true;
    }
    static public void main (String[] args)
    {
        Scanner myObj = new Scanner(System.in);
        String str;
        str = myObj.nextLine();
        if (isNumber(str))
            System.out.println("Yes");
        else
            System.out.println("No");
    }
}
