package kz.aitu.week1.weekp1;

import java.util.Scanner;
public class pracb3 {

    public void solve(int n, String start, String auxiliary, String end) {
        if (n == 1) {
            System.out.println(start + " -> " + end);
        } else {
            solve(n - 1, start, end, auxiliary);
            System.out.println(start + " -> " + end);
            solve(n - 1, auxiliary, start, end);
        }
    }
    public static void main(String[] args) {
        pracb3 a = new pracb3();
        Scanner scanner = new Scanner(System.in);
        int discs = scanner.nextInt();
        a.solve(discs, "A", "B", "C");
    }
}
