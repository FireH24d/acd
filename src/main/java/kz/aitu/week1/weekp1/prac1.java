package kz.aitu.week1.weekp1;
import java.util.Scanner;
public class prac1 {
    public static int findMinRec(int A[], int n)
    {
        int min = A[0];
        for(int i=1;i<A.length;i++){
            if(A[i] < min){
                min = A[i];
            }
        }
        return min;
    }
    public static void main(String args[])
    {
        int A[] = {10, 1, 32, 3, 45};
        int n = A.length;
        System.out.println(findMinRec(A, n));
    }
}


