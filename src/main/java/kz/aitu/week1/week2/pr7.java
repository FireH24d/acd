package kz.aitu.week1.week2;
import java.util.Scanner;
public class pr7 {
    public static void main(String[] args)
    {

        String s ;
        Scanner scan = new Scanner(System.in);
        s = scan.nextLine();
        int n = s.length();
        rec(s, 0, n-1);
    }
    private static void rec(String s, int a, int b)
    {
        if (a == b)
            System.out.println(s);
        else
        {
            for (int i = a; i <= b; i++)
            {
                s = swap(s,a,i);
                rec(s, a+1, b);
                s = swap(s,a,i);
            }
        }
    }
    public static String swap(String a, int i, int j)
    {
        char temp;
        char[] ch = a.toCharArray();
        temp = ch[i] ;
        ch[i] = ch[j];
        ch[j] = temp;
        return String.valueOf(ch);
    } }

