package kz.aitu.week1.week2;
import java.util.Scanner;
public class pr1 {
    public static int fibonacci( int n)
    {
        if(n==0) return 0;
        if(n==1) return 1;
        else return fibonacci(n-1) + fibonacci(n-2);
    }
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        System.out.println(fibonacci(n));
    }
}


