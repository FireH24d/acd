package kz.aitu.midterm2.First;


public class Node {
    Integer key;
    Node left;
    Node right;

    public Node(Integer key) {
        this.key = key;
        this.left = null;
        this.right = null;
    }

    public Node() {

    }

    public Node(Tree Tree) {
    }

    public Integer getKey() {
        return key;
    }

    public void setKey(Integer key) {
        this.key = key;
    }



    public Node getLeft() {
        return left;
    }

    public void setLeft(Node left) {
        this.left = left;
    }

    public Node getRight() {
        return right;
    }

    public void setRight(Node right) {
        this.right = right;
    }
}