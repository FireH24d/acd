package kz.aitu.week5.mt;
import lombok.Data;

public class Node {
    Node next;
     int data;

    public int getData() {
        return data;
    }

    public Node getNext() {
        return next;
    }

    public void setData(int data) {
        this.data = data;
    }

    public void setNext(Node next) {
        this.next = next;
    }

}

