package kz.aitu.week5.mt;
public class Main {
    public static void main(String[] args) {
        Stack stack1 = new Stack();
        Stack stack2 = new Stack();
        Stack stack3 = new Stack();
        stack1.push(1);
        stack1.push(2);
        stack1.push(17);
        stack1.push(18);
        stack1.push(19);

        stack2.push(1);
        stack2.push(13);
        stack2.push(14);
        stack2.push(15);

        stack3.push(17);
        stack3.push(6);
        stack3.push(20);
        stack3.push(21);


        Stack h = delete(stack1, stack2, stack3);
        Node a = h.getTop();
        while(a!=null){
            System.out.println(a.getData());
            a = a.getNext();
        }

    }
    public static Stack delete(Stack sk1, Stack sc2, Stack sc3) {
        Stack newStack = new Stack();
        int temp = 0;
        int w , s, d = 0;
        while ((sk1.getTop() != null) || (sc2.getTop() != null) || (sc3.getTop() != null)) {
            if (sk1.getTop() != null) {
                w = (sk1.getTop()).getData();
            } else {
                w = 0;
            }
            if (sc2.getTop() != null) {
                s = (sc2.getTop()).getData();
            } else {
                s = 0;
            }
            if (sc3.getTop() != null) {
                d = (sc3.getTop()).getData();
            } else {
                d = 0;
            }
            if ((w) >= (s) && (w) >= (d)) {
                temp = sk1.pop();
                newStack.push(temp);
            } else if ((s) >= (w) && (s) >= (d)) {
                temp = sc2.pop();
                newStack.push(temp);
            } else if ((d) >= (w) && (d) >= (s)) {
                temp = sc3.pop();
                newStack.push(temp);
            }
        }
        Stack a = new Stack();
        while (newStack.getTop() != null) {
            int current = newStack.pop();
            a.push(current);
        }
        return a;

    }
    }









