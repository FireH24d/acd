package kz.aitu.week5.mt;

public class Stack {
     Node top;
     int size = 0;
    public Node getTop(){
        return top;
    }
    public void push(int data){
        Node a = new Node();
        a.setData(data);
        if (size != 0) {
            a.setNext(top);
        }
        top = a;
        size++;
    }
    public int pop(){
        Node a = top;
        top = top.getNext();
        size--;
        return a.getData();
    }
    public boolean empty(){
        if (size == 0){
            return true;
        }
        else return false;
    }
    public int size(){
        return size;
    }
}




