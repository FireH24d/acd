package kz.aitu.week5.midterm.Second;


import lombok.Data;

@Data
public class Node{
        int data;
        Node next;
        Node(int tmp) {data = tmp;}
    }
