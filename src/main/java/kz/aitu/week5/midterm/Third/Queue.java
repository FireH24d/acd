package kz.aitu.week5.midterm.Third;

public class Queue {
    private Node head;
    private Node tail;

    public void setHead(Node head) {
        this.head = head;
    }

    public void setTail(Node tail) {
        this.tail = tail;
    }

    public Node getHead() {
        return head;
    }

    public Node getTail() {
        return tail;
    }

    public void push(Node block){
        if(head==null){
            head=block;
            tail=block;
        }
        else {
            tail.setNext(block);
            tail=block;
        }
    }

    public Node pop(){
        head=head.getNext();
        return head;
    }

    public int printGroup(Queue group) {//O(n)
        Node block = group.getHead();
        while (block != null) {
            if(block.getData().length()%2!=0){
                System.out.print(block.getData() + " ");
            }
            block = block.getNext();
        }
        System.out.println();
        return 0;
    }
}
