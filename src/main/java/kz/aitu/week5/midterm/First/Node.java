package kz.aitu.week5.midterm.First;

import lombok.Data;

@Data
public class Node{
    public Node next;
    public String data;

    public Node(String data){
        this.data = data;
    }

    public String data() {
        return data;
    }

    public String getData() {
return data;    }

    public Node next() {
        return next;
    }
    public void setNext(Node next) {
        this.next = next;
    }
    public Node getNext() {
        return next;
    }

    public String toString(){
        return this.data;
    }


}
