package kz.aitu.week5.midterm.First;


public class LinkedList {
    public Node head;
    public Node tail;

    public LinkedList(){
        this.head = new Node("head");
        tail = head;
    }

    public Node head() {
        return head;
    }

    public void add(Node node) {
        tail.next = node;
        tail = node;
    }

    public void pushback (String str) { //O(n)
        Node temp = new Node(str);
        tail.setNext(temp);
        tail = temp;
    }

    public void print(Node node) { //O(n)
        if(node == null) return;

        if(node != null){
            System.out.print(node.getData() + " ");
            node = node.getNext();
            print(node);
        }
    }

}