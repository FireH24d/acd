package kz.aitu.Bonus;

public class Main {
    public static void main(String[] args) {
        checkTask();
    }



    public static void checkTask() {
        BonusPoints bonusPoints = new BonusPoints();

        bonusPoints.addRoot("Student1");
        bonusPoints.addChild("Student2", "Student1");
        bonusPoints.addChild("Student3", "Student2");
        bonusPoints.addChild("Student4", "Student3");
        bonusPoints.addChild("Student5", "Student3");
        bonusPoints.addChild("Student6", "Student5");
        System.out.println(bonusPoints.getPoints("Student1")); //returns 93

        System.out.println(bonusPoints.getPoints("Student2")); //returns 93
        System.out.println(bonusPoints.getPoints("Student3")); //returns 93
        System.out.println(bonusPoints.getPoints("Student4")); //returns 93
        System.out.println(bonusPoints.getPoints("Student5")); //returns 93
        System.out.println(bonusPoints.getPoints("Student6")); //returns 93

    }


}
