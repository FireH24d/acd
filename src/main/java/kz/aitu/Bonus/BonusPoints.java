package kz.aitu.Bonus;

import javafx.scene.effect.Light;

import java.util.TreeMap;

public class BonusPoints {

    private HashTable hashTable;

    public BonusPoints() {
        hashTable = new HashTable(50);
    }

    public void addRoot(String name) {
        Node node = hashTable.get(name);
        if(node == null) hashTable.set(name);
    }

    public void addChild(String child, String parent) {
        Node nodeP = hashTable.get(parent);
        if(nodeP == null) {
            System.err.println("Parent: " + parent + " not found!");
            return;
        }
        Node nodeCh = hashTable.get(child);
        if(nodeCh != null) {
            System.err.println("Child: " + child + " is already done!");
            return;
        }
        nodeCh = new Node(child);
        if(nodeP.addNode(nodeCh)) {
            hashTable.set(child);
            System.out.println("Child: " + child + " has been added");
        } else {
            System.err.println("Parent: " + parent + " is already two children!");
            return;
        }
        System.out.println(hashTable.get("Student1"));
    }

    public int getPoints(String name) {
        Node node = hashTable.get(name);
        Node par=node;
                Node dfg=par.getLeft();
        Node asd=node;
        Node arr=asd.getRight();
        if(node == null) {
            return 0;
        }
        if((node.getLeft()==null&&node.getRight()==null)){
            return 80;
        }
        if((((dfg.getLeft()==null)&&(dfg.getRight()==null))&&(dfg!=null))){
            if(((arr==null))){

                return  80 + getPoints(node, 1,dfg)-gePoints(node,1,dfg);

            }
            return  80 + getPoints(node, 1,dfg);
        }

        return  80 + getPoints(node, 1,dfg);
    }

    private int getPoints(Node node, int level,Node text) {
        if(node == null) return 0;
        int point = 0;
        switch (level) {
            case 1: point = 5; break;
            case 2: point = 3; break;
            case 3: point = 2; break;
            default:point=0;
        }

        return getPoints(node.getLeft(), level+1,text )
                + getPoints(node.getRight(), level,text )
                + point;
    }

    private int gePoints(Node node, int level,Node text) {
        if((node.left == null)||(node.right==null)) return 3;


        return 0;
    }

}


