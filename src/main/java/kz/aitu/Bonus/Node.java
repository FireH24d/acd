package kz.aitu.Bonus;

import lombok.Data;

@Data
public class Node {
     String name;
     Node left;
     Node right;
     Node next;



    public Node(String name) {
        this.name = name;
        this.left = null;
        this.right = null;
        this.next = null;

    }

    public boolean addNode(Node next) {
        if(left == null) left = next;
        else if(right == null) right = next;
        else return false;
        return true;
    }
    public Node getlLeft(Node node) {
        return node.getLeft();
    }
    public Node getlRight(Node node) {
        return node.getRight();
    }

    public void setName(String name) {
        this.name = name;
    }
    public void setLeft(Node left) {
        this.left = left;
    }
    public String getName() {
        return name;
    }

    public Node getNext( ) {
        return next; }

    public Node getLeft() {
        return left;
    }
    public void setNext(Node a) {
        next = a;
    }

    public Node getRight() {
        return right;
    }



}
