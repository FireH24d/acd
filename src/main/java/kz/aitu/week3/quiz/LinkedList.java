package kz.aitu.week3.quiz;
public class LinkedList{ //nothing to change
    private Node head;
    private Node tail;

    public LinkedList(){
        this.head = new Node("head");
        tail = head;
    }

    public Node head(){
        return head;
    }

    public void add(Node node){
        tail.next = node;
        tail = node;
    }

    public void insertAt(String s, int index) {

        int i=0;
        Node n=new Node(s);
        Node c=head;
        while(i<index-1){
            c=c.next;
            i++;
        }
        n.next=c.next;
        n.next.data=c.next.data;

        c.next=n;
    }

    public void removeAt(int index) {

        Node n=head;
        while((--index)==0){
            n=n.next;
        }
        n.next=n.next.next;


    }}
