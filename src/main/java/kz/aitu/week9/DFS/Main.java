package kz.aitu.week9.DFS;

public class Main {
    public static void main(String[] args) {
        BinaryTree bt = new BinaryTree();

        bt.insert(1);
        bt.insert(2);
        bt.insert(3);
        bt.insert(9);
        bt.insert(8);
        bt.insert(7);
        bt.insert(6);
        bt.insert(5);
        System.out.println();
        bt.printDFS(bt);
    }
}
