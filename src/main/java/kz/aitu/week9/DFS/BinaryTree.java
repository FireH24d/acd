package kz.aitu.week9.DFS;

public class BinaryTree {
    public BinaryTreeNode root;

    public BinaryTree() {
        this.root = null;
    }

    public void insert(Integer data) {
        if (root == null) root = new BinaryTreeNode(data);
        else root.insert(data);
    }

    public BinaryTreeNode find(Integer data) {
        BinaryTreeNode node = findNode(this.root, data);
        if(node == null) return null;
        else return node;
    }

    private BinaryTreeNode findNode(BinaryTreeNode node, Integer key) {
        if(node == null) return null;
        if(key > node.data) return findNode(node.right, key);
        else if(key < node.data) return findNode(node.left, key);
        else return node;
    }
    public static void printDFS(BinaryTree bt){
        if (bt.root==null) return;
        Stack s = new Stack();
        s.push(bt.root.data);
        while(!s.empty()){
            Node current = s.pop();
            if (current.data==null) return;
            System.out.print(current.data+" ");
            BinaryTreeNode node = bt.find(current.data);
            if (node.right!=null) s.push(node.right.data);
            if (node.left!=null) s.push(node.left.data);
        }
    }
}
