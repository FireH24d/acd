package kz.aitu.week9.BFC;

public class Queue {
    private Node head;
    private Node tail;
    private int size;

    public void setTail(Node tail) {
        this.tail = tail;
    }

    public void setHead(Node head) {
        this.head = head;
    }

    public Node getTail() {
        return tail;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }

    public Node getHead() {
        return head;
    }

    public void add(int data) {
        Node node = new Node(data);
        if (head == null){
            head = node;
        } else {
            tail.next = node;
        }
        tail = node;
        size++;
    }

    public Node peak() {
        return this.head;
    }

    public void remove() {
        head = head.next;
        size--;
    }

    public boolean isEmpty() {
        return this.peak() == null;
    }

    public Node poll() {
        Node old = head;
        head = head.next;
        size--;
        return old;
    }

}
