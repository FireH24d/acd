package kz.aitu.week9.BFC;

public class BinaryTree {
    public BinaryTreeNode root;

    public void setRoot(BinaryTreeNode root) {
        this.root = root;
    }

    public BinaryTreeNode getRoot() {
        return root;
    }

    public void insert(int n) {
        BinaryTreeNode newNode = new BinaryTreeNode(n);
        if(root == null) {
            root = newNode;
            return;
        }
        BinaryTreeNode current = root;
        BinaryTreeNode parent = null;
        while(true) {
            parent = current;
            if(n < current.data){
                current = current.left;
                if(current == null){
                    parent.left = newNode;
                    return;
                }
            } else {
                current = current.right;
                if(current == null) {
                    parent.right = newNode;
                    return;
                }
            }
        }
    }

    public void display(BinaryTreeNode root) {
        if (root != null) {
            display(root.left);
            System.out.print(" " + root.data);
            display(root.right);
        }
    }
    public BinaryTreeNode find(int data) {
        return findNode(this.root, data);
    }
    private BinaryTreeNode findNode(BinaryTreeNode b, int ser) {
        if (b == null) {
            return null;
        }
        if (ser > b.data) {
            return findNode(b.right, ser);
        } else if (ser < b.data) {
            return findNode(b.left, ser);
        } else {
            return b;
        }
    }
    public static void bfs(BinaryTree t){
        if (t.getRoot() == null) {
            return;
        }
        Queue q = new Queue();
        q.add(t.getRoot().getData());
        while(!q.isEmpty()) {
            Node temp = q.poll();
            System.out.print(temp.getData() + " ");
            BinaryTreeNode b = t.find(temp.getData());
            if (b.getLeft() != null) {
                q.add(b.getLeft().getData());
            }
            if (b.getRight() != null) {
                q.add(b.getRight().getData());
            }
        }
    }


}
