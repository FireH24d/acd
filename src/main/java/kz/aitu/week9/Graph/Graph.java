package kz.aitu.week9.Graph;

public class Graph<Key, Value> {
    public HashTable vertexTable;
    public boolean bidirectional = false;

    public void addVertex(Key key, Value value) {
        Vertex vertex = new Vertex<Key, Value>(key, value);

        if(vertexTable.containsKey(key)) return;
        else vertexTable.put(key, vertex);
    }

    public void addEdge(Key key1, Key key2) {
        Vertex vertexA = vertexTable.get(key1);
        Vertex vertexB = vertexTable.get(key2);

        vertexA.addEdge(vertexB);
        if (bidirectional) {
            vertexB.addEdge(vertexA);
        }
    }

    public boolean isConnected(Key key1, Key key2) {
        Vertex vertex1 = vertexTable.get(key1);
        Vertex vertex2 = vertexTable.get(key2);
        if (vertex1.connectedWith(vertex2)) return true;
        return false;
    }

    public int countVertices() {
        if (vertexTable == null) {
            return 0;
        }
        return vertexTable.countVertices();
    }

    public void printAll() {
        vertexTable.printAll();
        System.out.println();
    }

    public boolean isBidirectional() {
        return bidirectional;
    }

    public void setBidirectional(boolean bidirectional) {
        this.bidirectional = bidirectional;
    }

    public void setVertexTable(HashTable vertexTable) {
        this.vertexTable = vertexTable;
    }

    public HashTable getVertexTable() {
        return vertexTable;
    }
}

