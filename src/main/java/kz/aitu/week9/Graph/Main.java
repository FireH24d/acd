package kz.aitu.week9.Graph;

public class Main {
    public static void main(String[] args) {
        Graph graph = new Graph<Integer, String>();
        graph.addVertex(5,"Alisher");
        graph.addVertex(7,"Arman");
        graph.addVertex(1,"Abat");
        graph.addVertex(3,"Adilkhan");
        graph.addEdge(3,1);
        graph.addEdge(5,7);
        graph.addEdge(3,5);
        System.out.println(graph.isConnected(3,5));
        graph.printAll();
    }
}
