package kz.aitu.week9.Graph;

public class Vertex<Key, Value> {
    public Key key;
    public Value value;
    public LinkedList edges;
    public Vertex next;
    public Vertex nextL;

    public Vertex(Key key, Value value){
        this.key = key;
        this.value = value;
        edges = new LinkedList();
    }

    public void addEdge(Vertex vertex) {
        if(!edges.contains(vertex)) {
            edges.add(vertex);
        }
    }

    public boolean equals(Vertex o) {
        if(o == null) {
            return false;
        }
        if(this.key == o.key) {
            return true;
        }
        return false;
    }

    public boolean connectedWith(Vertex vertex2) {
        if (edges.isEmpty()) {
            return false;
        } else {
            return edges.connectedWith(vertex2);
        }
    }

    public void setKey(Key key) {
        this.key = key;
    }

    public void setEdges(LinkedList edges) {
        this.edges = edges;
    }

    public void setValue(Value value) {
        this.value = value;
    }

    public Key getKey() {
        return key;
    }

    public Value getValue() {
        return value;
    }

    public LinkedList getEdges() {
        return edges;
    }

    public void setNext(Vertex next) {
        this.next = next;
    }

    public Vertex getNext() {
        return next;
    }

    public void setNextL(Vertex nextL) {
        this.nextL = nextL;
    }

    public Vertex getNextL() {
        return nextL;
    }
}

