package kz.aitu.bst;

public class BSTree {
     Node root;

    public BSTree() {
        this.root = null;
    }

    public void insert(Integer key, String value) {
        Node node = new Node(key, value);
        if(root == null) {
            root = node;
            return;
        } else {
            Node current = root;
            Node parent = null;
            while (true) {
                parent = current;
                if (key < current.getKey()) {
                    current = current.getLeft();
                    if (current == null) {
                        parent.setLeft(node);
                        return;
                    }
                } else {
                    current = current.getRight();
                    if (current == null) {
                        parent.setRight(node);
                        return;
                    }
                }
            }
        }

    }
    public Node deleteNode(Node root, int value){
        if(root == null) return null;
        if(value < root.key){
            root.setLeft(deleteNode(root.getLeft(), value));
        }else if(value > root.getKey()){
            root.setRight(deleteNode(root.getRight(), value));
        }
        else{
            if(root.getLeft() == null && root.getRight() == null){
                return null;
            }else if(root.getLeft() == null){
                return root.getRight();
            }else if(root.getRight() == null){
                return root.getLeft();
            }
            else {
                String str = rooot(root.getRight());
                root.setValue(str);
                root.setKey(notdoub(root.getRight()));
                root.setRight(deleteNode(root.getRight(),notdoub(root.getRight())));
            }
        }
        return root;
    }
    int notdoub(Node node) {
        if((node.getLeft() != null)){
            return notdoub(node.getLeft());
        }
        return node.getKey();
    }
    String rooot(Node node){
        if(node.getLeft() !=null){
            return rooot(node.getLeft());
        }
        return node.getValue();
    }


    public String find(Integer key) {
        Node node = findNode(root, key);
        if(node == null) return null;
        else return node.getValue();
    }

    private Node findNode(Node node, Integer key) {
        if(node == null) return null;
        if(key > node.getKey()) return findNode(node.getRight(), key);
        else if(key < node.getKey()) return findNode(node.getLeft(), key);
        else return node;
    }

    public String findWithoutRecursion(Integer key) {
        Node current = root;
        while(current!=null) {
            if(key > current.getKey()) current = current.getRight();
            else if(key < current.getKey()) current = current.getLeft();
            else return current.getValue();
        }
        return null;
    }


    public void printAllAscending() {
        print(root);
        System.out.println("");

    }
    private void print(Node root) {
        if(root == null) return;
        print(root.getLeft());
        System.out.print(root.getValue());
        print(root.getRight());
    }


    public void printAll() {
        for(int i = 1; i<=size(root); i++)
        { printt(root ,i); }
    }
    static int size(Node root)
    {
        if(root == null)
            return 0;
        int l = size(root.left);
        int r = size(root.right);
        return Math.max(l + 1 ,r + 1);
    }

    static void printt(Node root,int i)
    {
        if(root == null)
            return;
        if(i == 1)
            System.out.print(root.value) ;
        else if(i > 1)
        {
            printt(root.left ,i - 1);
            printt(root.right ,i - 1);
        }
    }

}





