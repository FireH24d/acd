package kz.aitu.week4.queue;


public class Queue {
    Node head;
    Node tail;


    public void add(int data) {
       Node a = new Node(data);
        if (head == null) {
            head = a;
        } else {
            Node t=head;
            head=a;
            a.next = t;
        }
    }

    public Node poll() {
        Node a = head;
        if (head == null) {
            return null;
        } else {

            head=head.next;
            return a;
        }


    }

    public int size() {
        Node current = head;
        int i=0;
        if (head == null) {
            return 0;
        } else {
            while (current != null) {
                current = current.next;
                i++;
            }
        }
        return i;
    }

    public int peek() {

        if (tail!=null){

return 0;
        }
       else return head.data;

    }
}
