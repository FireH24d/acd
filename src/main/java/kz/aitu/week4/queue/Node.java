package kz.aitu.week4.queue;

import lombok.Data;

@Data
public class Node {
    int data;
    Node next;

    public Node(int data) {
        this.data = data;
    }
    public Node getNext() {
        return next;
    }

}

