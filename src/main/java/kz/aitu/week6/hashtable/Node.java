package kz.aitu.week6.hashtable;

import lombok.Data;

@Data
public class Node {
    public String data;
    public String key;
    Node next;
    public Node(String key,String data){
        this.data=data;
        this.key=key;
    }
}





