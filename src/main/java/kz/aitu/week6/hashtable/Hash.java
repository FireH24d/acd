package kz.aitu.week6.hashtable;

public class Hash {
     Node tail;
     int size;
     Node arr[];
    public Hash(int size){
        this.tail=null;
        this.size=size;
        arr = new Node[size];

    }

    public void add(String key,String data){
        int code = key.hashCode();
        int index=code%size;
        Node node=new Node(key, data);

        if (arr[index]==null){
            arr[index]=node;
            tail=node;
        }else{
            tail.next=node;
            tail=node;
        }
    }
    public String get(String key){
        int index=hashCode()%size;

        if (arr[index]==null) return null;
        int code = key.hashCode();
        return arr[index].data;
    }
    public void remove(String key){
        int code = key.hashCode();
        int index=code%size;
        arr[index]=null;

    }
    public void print(){

        for (int i=0;i<size;i++){
            Node temp=arr[i];
            while (temp!=null){
                System.out.println(temp.data);
                temp=temp.next;
            }
        }
    }
    public int Size(){
        int j=0;
        for (int i=0;i<size;i++){
            Node temp=arr[i];
            while (temp!=null){
                j++;
                temp=temp.next;
            }
        }
        return j;
    }
}


