package kz.aitu.week6.tree;

public class Tree {
    Node root;

    public void push(Node node, int data) {
        if (data < node.value) {
            if (node.left != null) {
                push(node.left, data);
            }
            else {
                node.left = new Node(data);
            }
        }
        else if (data > node.value) {
            if (node.right != null) {
                push(node.right, data);
            } else {
                node.right = new Node(data);
            }
        }
    }
    public void traverseInOrder(Node node) {
        if (node != null) {
            traverseInOrder(node.left);
            System.out.print(" " + node.value);
            traverseInOrder(node.right);
        }
    }
    public void traversePreOrder(Node node) {
        if (node != null) {
            System.out.print(" " + node.value);
            traversePreOrder(node.left);
            traversePreOrder(node.right);
        }
    }
    public void traversePostOrder(Node node) {
        if (node != null) {
            traversePostOrder(node.left);
            traversePostOrder(node.right);
            System.out.print(" " + node.value);
        }
    }

}
