package kz.aitu.week6.tree;

import lombok.Data;

@Data
public class Node {
    int value;
    Node left, right;

    Node(int value){
        this.value = value;
        left = null;
        right = null;
    }
}
